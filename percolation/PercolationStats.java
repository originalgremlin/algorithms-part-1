public class PercolationStats {
    private double[] results;
    
    // perform T independent computational experiments on an N-by-N grid
    public PercolationStats(int N, int T) {
        if (N <= 0)
            throw new IllegalArgumentException("N <= 0");
        if (T <= 0)
            throw new IllegalArgumentException("T <= 0");
        results = new double[T];
        for (int i = 0; i < T; i++)
            results[i] = doMonteCarlo(N);
    }
    
    private double doMonteCarlo(int N) {
        Percolation p = new Percolation(N);
        // open up blocked sites at random until the system percolates
        while (!p.percolates()) {
            p.open(StdRandom.uniform(N) + 1, StdRandom.uniform(N) + 1);
        }
        // calculate number of open sites
        int count = 0;
        for (int i = 1; i <= N; i++) {
            for (int j = 1; j <= N; j++) {
                if (p.isOpen(i, j))
                    count++;
            }
        }
        return ((double) count) / (N * N);
    }
    
    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(this.results);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(this.results);
    }
    
    // returns lower bound of the 95% confidence interval
    public double confidenceLo() {
        return mean() - 1.96 * stddev() / Math.sqrt(results.length);
    }
   
    // returns upper bound of the 95% confidence interval
    public double confidenceHi() {
        return mean() + 1.96 * stddev() / Math.sqrt(results.length);
    }
    
    // test client, described below
    public static void main(String[] args) {
        int N = new Integer(args[0]);
        int T = new Integer(args[1]);
        PercolationStats ps = new PercolationStats(N, T);
        StdOut.printf("%-23s = %f%n", "mean", ps.mean());
        StdOut.printf("%-23s = %f%n", "stddev", ps.stddev());
        StdOut.printf("%-23s = %f, %f%n", "95% confidence interval",
                      ps.confidenceLo(),
                      ps.confidenceHi());
    }
}