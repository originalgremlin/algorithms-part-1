public class Percolation {
    private int size;
    private boolean[][] grid;

    private int virtualTop, virtualBottom;
    private WeightedQuickUnionUF uf;
    
    public Percolation(int N) {
        // create N-by-N grid, with all sites blocked
        // blocked = false; open = true
        size = N;
        grid = new boolean[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                grid[i][j] = false;
            }
        }
        // create N-by-N quick union object
        // with 2 extra "virtual" sites on top and bottom
        virtualTop = 0;
        virtualBottom = (N * N) + 1;
        uf = new WeightedQuickUnionUF(virtualBottom + 1);
        // connect virtual sites to top/bottom rows
        for (int i = 1; i <= N; i++)
            uf.union(virtualTop, i);
        for (int i = (N * N) - N + 1; i <= N * N; i++)
            uf.union(virtualBottom, i);
    }
    
    private void checkBounds(int i, int j) {
        if (i < 1 || i > size)
            throw new IndexOutOfBoundsException("row index i out of bounds");
        if (j < 1 || j > size)
            throw new IndexOutOfBoundsException("column index j out of bounds");
    }
    
    private int gridToUF(int i, int j) {
        checkBounds(i, j);
        return ((i - 1) * size) + j;
    }
        
    public void open(int i, int j) {
        checkBounds(i, j);
        if (!isOpen(i, j)) {
            // open site (row i, column j) if it is not already
            grid[i - 1][j - 1] = true;
            // connect the site to its neighbors
            int k = gridToUF(i, j);
            if (i > 1 && isOpen(i - 1, j))
                uf.union(k, k - size);
            if (i < size && isOpen(i + 1, j))
                uf.union(k, k + size);
            if (j > 1 && isOpen(i, j - 1))
                uf.union(k, k - 1);
            if (j < size && isOpen(i, j + 1))
                uf.union(k, k + 1);
        }
    }
    
    // is site (row i, column j) open?
    public boolean isOpen(int i, int j)  {
        checkBounds(i, j);
        return grid[i - 1][j - 1];
    }
    
    // is site (row i, column j) full?
    public boolean isFull(int i, int j) {
        checkBounds(i, j);
        return isOpen(i, j) && uf.connected(virtualTop, gridToUF(i, j));
    }
    
    // does the system percolate?
    public boolean percolates() {
        return uf.connected(virtualTop, virtualBottom);
        /*
        for (int j = 1; j <= size; j++) {
            if (uf.connected(virtualTop, gridToUF(size, j)))
                return true;
        }
        return false;
        */
    }
}